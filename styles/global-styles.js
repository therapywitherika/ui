import { css } from '../core/lit-core.min.js';

export const globalStyles = css`
    a {
        color: rosybrown;
        cursor: pointer;
        text-decoration: none;
    }
    a:hover {
        color: palevioletred;
    }
    footer {
        background: rgb(0, 0, 0);
        background: rgba(0, 0, 0, 0.75);
        right: 0px;
        color: white;
        font-size: large;
        padding: 24px;
        position: fixed;
        top: 115px;
        height: 212px;
        z-index: 100;
    }
    h2 {
        color: black;
        font-size: x-large;
        font-weight: 400;
        margin: 8px 0 48px 0;
        text-align: center;
    }
    i {
        font-size: small;
    }
    header {
        background-color: white;
        box-shadow: 0px 2px 4px 0px rosybrown;
        font-family: 'Fjalla One', sans-serif;
        font-size: xxx-large;
        font-weight: 400;
        padding: 8px 0 0 0;
        position: sticky;
        text-align: center;
        top: 0px;
        z-index: 100;
    }
    header .image {
        background-color: transparent;
        background-image: url(images/Blossom-Purple.jpg);
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: cover;
        height: 260px;
    }
    article {
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 490px;
        width: 800px;
    }
    main {
        display: flex;
        font-family: 'Montserrat', sans-serif;
        font-size: large;
        justify-content: center;
    }
    nav {
        padding: 8px 0px;
        text-align: center;
    }
    nav a {
        font-size: x-large;
        padding: 24px;
        text-transform: uppercase;
    }
    section {
        color: #666;
        padding: 16px;
    }
    table {
        border-collapse: collapse;
        margin-bottom: 16px;
        padding: 0px;
        width: 100%;
    }
    table td {
        font-size: medium;
        padding: 8px;
        vertical-align: top;
    }
    table th {
        background-color: #333;
        color: white;
        font-size: large;
        padding: 8px;
        text-align: left;
    }
    table tr:nth-child(even) {
        background-color: #eee;
    }
    .address {
        font-size: medium;
    }
    .contact a {
        color: palevioletred;
        font-size: medium;
        font-weight: 500;
    }
    .contact a:hover {
        color: rosybrown;
    }
    .container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
    }
    .icon {
        align-items: center;
        display: flex;
    }
    .item {
        flex-basis: 1;
        flex-grow: 1;
    }
    .item.left {
        padding-right: 16px;
    }
    .name {
        font-weight: 500;
    }
    .title {
        font-size: medium;
        font-style: italic;
        margin-bottom: 8px;
    }
    .material-icons {
        font-size: 20px;
    }
`;
