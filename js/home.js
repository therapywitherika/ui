import { html, LitElement } from '../core/lit-core.min.js';
import { globalStyles } from '../styles/global-styles.js';

class AppHome extends LitElement {
    constructor() {
        super();
    }

    static styles = [globalStyles];

    render() {
        const { credentials, email, sessions, specialties } = this;

        return html`
            <section>
                <h2 id="home">Welcome to the Rest of Your Life</h2>
                <p>
                    Life is a journey, with challenges throughout, and can be overwhelming to navigate on your own.
                    Therapy is a powerful tool to help you cope with challenges and thrive.
                </p>
                <p>
                    I strongly believe that a therapeutic connection can be life-changing and would love to help you on
                    your journey to well-being. I approach treatment based upon client needs with a focus on attachment,
                    strength, resilience, and human connection.
                </p>
                <p>Please <a href="mailto: ${email}">contact me</a> to see if I would be a good fit for you.</p>
            </section>
            <section>
                <h2 id="about">About Erika</h2>
                <p>
                    I am passionate about helping people find the strength they possess and empowering them to better
                    their lives. I am client-centered and adapt treatment to suit each person, couple or family, based
                    upon their goals. I have a lot of experience helping people who have suffered relational trauma,
                    particularly adult children of narcissists.
                </p>
                <p>
                    After graduate school, I was invited to intern under one of my professors in a private practice
                    setting. I also worked in a community based non-profit providing services to children in their
                    schools or homes, as well as provided group therapy in a partial hospitalization program.
                </p>
                <p>I am certified in TFCBT (Trauma Focused Cognitive-Behavioral Therapy).</p>
                <p>
                    I have been seeing clients since April 2011. The California Board of Behavioral Sciences issued my
                    registered Marriage and Family Therapist Intern Number (IMF70110) in March 2012 and I became a
                    Licensed Marriage and Family Therapist in July 2016 (94327).
                </p>
                <p>Therapy services are offered to clients in California through telehealth.</p>
            </section>
            <section>
                <h2 id="services">Treatment Information</h2>
                <p>
                    ​Our first visit will give us an opportunity to develop goals based upon your needs and determine
                    how we can best work together to help you.
                </p>
                <table class="session">
                    <thead>
                        <tr>
                            <th>Session</th>
                            <th>Length</th>
                            <th>Rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${sessions.map(
                            ({ description, length, rate }) => html`<tr>
                                <td>${description}</td>
                                <td>${length}</td>
                                <td>${rate}</td>
                            </tr>`
                        )}
                    </tbody>
                </table>
                <p>
                    <i>Evening appointments are available. All appointments are through telehealth.</i>
                </p>
                <div class="container">
                    <div class="item left">
                        <table>
                            <thead>
                                <tr>
                                    <th>Specialties</th>
                                </tr>
                            </thead>
                            <tbody>
                                ${specialties.map(
                                    specialty => html`<tr>
                                        <td>${specialty}</td>
                                    </tr>`
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div class="item">
                        <table>
                            <thead>
                                <tr>
                                    <th>Insurance</th>
                                </tr>
                            </thead>
                            <tbody>
                                ${credentials.map(
                                    credential => html`<tr>
                                        <td>${credential}</td>
                                    </tr>`
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        `;
    }
}
customElements.define('app-home', AppHome);
