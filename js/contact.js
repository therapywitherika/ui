import { html, LitElement } from '../core/lit-core.min.js';
import { globalStyles } from '../styles/global-styles.js';

class AppContact extends LitElement {
    static properties = {
        email: { type: String },
        license: { type: Object },
        phone: { type: String }
    };

    static styles = [globalStyles];

    constructor() {
        super();

        this.email = '';
        this.license = {};
        this.phone = '';
    }

    render() {
        const {
            email,
            license: { link, number, target },
            phone
        } = this;

        return html`
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
            <div class="icon name">Erika Kincaid, M.A.</div>
            <div class="title">Licensed Marriage and Family Therapist, ${number}</div>
            <div class="contact icon">
                <i class="material-icons">email</i>&nbsp;<a href="mailto: ${email}">${email}</a>
            </div>
            <div class="contact icon">
                <i class="material-icons">phone</i>&nbsp;<a href="tel: ${phone}">${phone}</a>
            </div>
            <div class="contact icon">
                <i class="material-icons">badge</i>&nbsp;<a href=${link} target=${target}>License ${number}</a>
            </div>
        `;
    }
}
customElements.define('app-contact', AppContact);
