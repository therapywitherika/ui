import { html, LitElement } from '../core/lit-core.min.js';
import { offsets } from '../core/config.js';
import { globalStyles } from '../styles/global-styles.js';

class AppHeader extends LitElement {
    static properties = {
        title: { type: String }
    };

    constructor() {
        super();
        this.title = '';
    }

    static styles = [globalStyles];

    _handleOffset(e, section) {
        document.body.scrollTo({ behavior: 'smooth', top: offsets[section] });
    }

    render() {
        const { _handleOffset, sections, title } = this;

        return html`
            <header>
                ${title}
                <nav>${sections.map(({ label }) => html`<a @click=${e => _handleOffset(e, label)}>${label}</a>`)}</nav>
                <div class="image"></div>
            </header>
        `;
    }
}

customElements.define('app-header', AppHeader);
