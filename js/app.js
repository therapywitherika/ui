import { html, LitElement } from '../core/lit-core.min.js';
import { data } from '../core/config.js';
import { globalStyles } from '../styles/global-styles.js';
import '../js/contact.js';
import '../js/header.js';
import '../js/home.js';

export class AppMain extends LitElement {
    constructor() {
        super();
    }

    static styles = [globalStyles];

    render() {
        const { credentials, email, license, phone, sections, sessions, specialties, title } = data;

        return html`
            <app-header
                email=${email}
                .license=${license}
                phone=${phone}
                .sections=${sections}
                title="${title}"
            ></app-header>
            <main>
                <article>
                    <app-home
                        .credentials=${credentials}
                        email=${email}
                        .sessions=${sessions}
                        .specialties=${specialties}
                    ></app-home>
                </article>
                <footer>
                    <app-contact email=${email} .license=${license} phone=${phone}></app-contact>
                </footer>
            </main>
        `;
    }
}
customElements.define('app-main', AppMain);
