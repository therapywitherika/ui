export const data = {
    credentials: ['Aetna', 'Optum'],
    email: 'Erika@TherapyWithErika.com',
    license: {
        link: 'https://search.dca.ca.gov/details/2001/LMFT/94327/4adbacec9d167bd2bcd57d4fd0637d3f',
        number: '#94327',
        target: '_blank'
    },
    phone: '949-334-7878',
    sections: [{ label: 'home' }, { label: 'about' }, { label: 'services' }],
    sessions: [
        { description: 'Intake', length: '', rate: '$175.00' },
        { description: 'Individual', length: '45 Minutes', rate: '$130.00' },
        { description: 'Individual', length: '60 Minutes', rate: '$160.00' }
    ],
    specialties: [
        'Adult Children of Narcissists',
        'Anxiety',
        'Depression',
        'Healthy Relationships',
        'Self-Esteem',
        'Trauma'
    ],
    title: 'THERAPY WITH ERIKA'
};

export const offsets = { about: 300, home: 0, services: 835 };
